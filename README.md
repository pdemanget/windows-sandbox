SANDBOX WITH SCOOP
==================

This is a Proof Of Concept usage for scoop within Windows Sandbox.

To run the sandbox just double click "sandbox.wsb"

Windows Sandbox usage
----------------------
You can see the definition of shared folders within the wsb file, I'm sharing some of my host application to avoid reinstalling everything everytime I start the sandbox.

I need to tweak git so that it can run in another folder, so I have a startup file: sandbox_rc.bat, and a local .gitconfig for the sandbox configuration.


Scoop usage
--------

I'm using scoop and my own scoop bucket to install my own application in a sandboxed. 
Basicaly, the main usage is to test my installers. 


Technical doc
=============
Shared folders
--------------
For optimisation (and limitations) we reuse the installed apps of the host: git, 7z, but not scoop which is reinstalled


Startup sequence
----------------
- sandbox.wsb: share folders, startup cmd file
- sandbox_rc.bat: variables, launch all other startup files
- sandbox_postinstall.ps1: installs SCOOP
- sandbox_userinstall.bat: installs batrun with scoop

