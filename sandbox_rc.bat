@Echo off
rem Main sandbox install
rem This file will use the share git and 7zip install of the host
rem then install scoop and you scoop Software
rem you then rely on scoop configuration to configure your non empty Windows Sandbox

rem postinstall of the sandbox Id Est the main startup
echo %dp0;
rem path scoop\apps\7zip\23.01 scoop\shims
setx SCOOP_HOME C:\scoop\
set SCOOP_HOME= C:\scoop\
set WORK_HOME=c:\private\sandbox
setx GIT_CONFIG_GLOBAL c:\private\sandbox\.gitconfig
rem desktop folder C:\Users\WDAGUtilityAccount\Desktop\
rem shim doesn't work
setx PATH_ADDITIONS "C:\programs\Git\cmd;%SCOOP_HOME%\apps\7zip\23.01;%SCOOP_HOME%\apps\scoop\current"

rem does it work?
setx PATH "%PATH%;C:\programs\Git\cmd;%SCOOP_HOME%\apps\7zip\23.01"

rem reg add "HKCU\Software\Microsoft\Command Processor" /v Autorun /d "%WORK_HOME%\first-launch.bat" /f

PowerShell -Command "Set-ExecutionPolicy Unrestricted -Scope CurrentUser"
start notepad %WORK_HOME%\WAIT.md

PowerShell %WORK_HOME%\sandbox_postinstall.ps1


start notepad %WORK_HOME%\HOME.md

rem env
set GIT_CONFIG_GLOBAL=c:\private\sandbox\.gitconfig
rem re-set temporary  variables for 1st run
set SCOOP_HOME=C:\scoop\
set PATH=C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;%USERPROFILE%\scoop\shims;%USERPROFILE%\AppData\Local\Microsoft\WindowsApps;C:\programs\Git\cmd;C:\scoop\apps\7zip\23.01	

start cmd /K %WORK_HOME%\sandbox_userinstall.bat