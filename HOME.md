SANDBOX WITH SCOOP
==================

If you wait to see screen, scoop is installed, and the postinstall has executed.



For information the manual operation that should be executed (in powershell):

    Set-ExecutionPolicy Unrestricted -Scope CurrentUser
    Invoke-RestMethod -Uri https://get.scoop.sh | Invoke-Expression
    scoop bucket add batrun https://gitlab.com/pdemanget/batrun
    scoop install batrun/batrun


Old scoop instalation instruction
----------------------------------
    Set-ExecutionPolicy Unrestricted -Scope CurrentUser
    iwr -useb get.scoop.sh | iex

General path location
----------------------
C:\Users\WDAGUtilityAccount\scoop\shims\scoop.cmd

PATH=C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Users\WDAGUtilityAccount\scoop\shims;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Users\WDAGUtilityAccount\AppData\Local\Microsoft\WindowsApps;C:\programs\Git\cmd; C:\scoop\\apps\7zip\23.01
